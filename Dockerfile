FROM 		    	ubuntu:yakkety

MAINTAINER 		Engineering At The Lathe, LLC <engineering@thelathe.com>

ENV 		    	LANG C.UTF-8

RUN 		    	apt-get update
RUN		      	apt-get install -y --no-install-recommends apt-utils bzip2 unzip vim xz-utils
RUN         		apt-get install -y authbind
RUN     	  	apt-get install -y openjdk-8-jre-headless
RUN 		    	apt-get install -y tomcat7 tomcat7-admin

RUN		      	mkdir -p /etc/betaseron/data_exchange/import/process

COPY		    	conf/lib/mail-1.4.7.jar /usr/share/tomcat7/lib/mail-1.4.7.jar
COPY        		conf/config/tomcat-users.xml /etc/tomcat7/tomcat-users.xml
COPY        		conf/config/server.xml /etc/tomcat7/server.xml
COPY        		conf/config/tomcat7 /etc/default/tomcat7

ADD 		    	conf/scripts/run.sh /root/run.sh
RUN 		    	chmod +x /root/run.sh

RUN         		chown -R tomcat7:tomcat7 /var/lib/tomcat7
RUN     	  	chown -R tomcat7:tomcat7 /etc/tomcat7
RUN 		    	chown -R tomcat7:tomcat7 /usr/share/tomcat7
RUN 		    	chown -R tomcat7:tomcat7 /etc/betaseron

RUN         		touch /etc/authbind/byport/80
RUN         		chmod 500 /etc/authbind/byport/80
RUN         		chown tomcat7 /etc/authbind/byport/80

EXPOSE  	  	8080
EXPOSE      		8005
EXPOSE      		80

CMD 		    	["/root/run.sh"]
