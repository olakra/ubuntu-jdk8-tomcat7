# Docker

### Related Links:
- [Official Docker documents](https://docs.docker.com/)
- [Official Docker hub](https://hub.docker.com/)
- [OpenJDK Docker images](https://hub.docker.com/_/openjdk/)
- [Ubuntu Docker images](https://hub.docker.com/_/ubuntu/)
- [Using Docker](https://thelathe.atlassian.net/wiki/pages/viewpage.action?pageId=72876036)

### Managing the docker container:

1. Create a docker **image** from the project's _**Dockerfile**_[^1]:

	The following command creates an image named _betaseron_, using the dockerfile configurations.

	```
	$ docker build -t betaseron .
	```

	For more details about the **build** please see the [official documentation](https://docs.docker.com/engine/reference/commandline/build)

2. Create and start a docker **container** from the _image_ created in the previous step:

	When creating a container it is very important to ensure that the **ports** are correctly mapped between the container and the host machine.

	The following command creates a named container (_betaseron_) and starts it. It maps the host machine's port 8080 to the container's _exposed_ port 8080

	```
	$ docker run -d --name betaseron -p 8080:8080 betaseron
	```

	For more details about the **run** please see the [official documentation](https://docs.docker.com/engine/reference/commandline/run)

3. The following command shows how to stop the running container:

	```
	$ docker stop betaseron
	```

	For more details about the **stop** please see the [official documentation](https://docs.docker.com/engine/reference/commandline/stop)

4. The following command deletes docker containers:

	**NOTE**: only containers that are **not** currently running can be deleted.

	```
	$ docker rm $(docker ps -a -q)
	```

	For more details about the **rm** please see the [official documentation](https://docs.docker.com/engine/reference/commandline/rm)

5. The following command deletes docker images:

	**NOTE**: only images that currently **don't** have running containers can be deleted.

	```
	$ docker rmi $(docker images -q)
	```

	For more details about the **rmi** please see the [official documentation](https://docs.docker.com/engine/reference/commandline/rmi)


### Configuring Maven for deploying to docker:

1. Ensure that your _pom.xml_ has the following:

	a. Profile _docker_ with the tomcat user's [access credentials](https://tomcat.apache.org/tomcat-7.0-doc/manager-howto.html#Configuring_Manager_Application_Access)

	b. Profile _no-proxy_

	The following command builds the project and uploads the _ROOT.war_ to the docker container

	```
	$ mvn -Pno-proxy -Pdocker -Dmaven.test.skip=true clean install tomcat7:deploy
	```

2. Verify the installation

	[http://localhost:8080](http://localhost:8080/)


# Footnotes:

[^1]: The assumption is that you are executing these command from the project directory.
